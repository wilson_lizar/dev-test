[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MvcApplication.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(MvcApplication.App_Start.NinjectWebCommon), "Stop")]

namespace MvcApplication.App_Start
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using System.Web.Http;
    using System.Configuration;
    using AutoMapper;
    using Framework;
    using System.Reflection;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);

                // set the dependency resolver
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            // binding repositories
            kernel.Bind<Business.Repository.IRestaurantRepository>().To<DataAccess.RestaurantRepository>()
                .WithConstructorArgument("connectionString", cfg => ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString);

            // binding services
            kernel.Bind<Business.Service.IRestaurantService>().To<BusinessService.RestaurantService>();

            InitAutoMapper(kernel);
        }

        private static void InitAutoMapper(IKernel kernel)
        {
            kernel.Bind<IMappingEngine>().ToMethod(context => Mapper.Engine);

            var mappings = AppDomain.CurrentDomain.GetAssemblies().ToList()
                .SelectMany(a => a.GetTypes())
                .Where(t => !t.IsAbstract &&
                            typeof(IObjectMapping).IsAssignableFrom(t))
                .Select(Activator.CreateInstance)
                .Cast<IObjectMapping>();

            foreach (IObjectMapping mapping in mappings)
                mapping.InitMapping(Mapper.Configuration);
        }
    }
}
