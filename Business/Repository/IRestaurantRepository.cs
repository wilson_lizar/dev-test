﻿using Business.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public interface IRestaurantRepository
    {
        IEnumerable<Restaurant> GetAll();

        Restaurant Get(int id);

        bool Update(Restaurant restaurant);
    }
}
