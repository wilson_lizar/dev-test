﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Framework;
using Framework.Extension;

namespace MvcApplication.Mapping
{
    public class ModelMapping : IObjectMapping
    {
        public void InitMapping(object config)
        {
            var automapConfig = config as AutoMapper.IConfiguration;
            if (automapConfig != null)
            {
                // mapping from View model to Domain model
                automapConfig.CreateMap<Models.RestaurantModel, Business.DomainModel.Restaurant>()
                    .ForMember(dest => dest.PhoneNumberKey, opt => opt.Ignore())
                    .ForMember(dest => dest.Photo, opt => opt.MapFrom(src => src.Photo != null ? src.Photo.InputStream.ReadBytes() : null));

                // mapping from Domain model to View model
                automapConfig.CreateMap<Business.DomainModel.Restaurant, Models.RestaurantModel>()
                    .ForMember(dest => dest.Photo, opt => opt.Ignore())
                    .ForMember(dest => dest.HasPhoto, opt => opt.MapFrom(src => src.Photo != null));
            }
        }
    }
}