﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Framework;
using Business.DomainModel;

namespace DataAccess.Mapping
{
    public class ObjectMapping : IObjectMapping
    {
        public void InitMapping(object config)
        {
            var automapConfig = config as AutoMapper.IConfiguration;
            if (automapConfig != null)
            {
                // mapping from Domain model to Db model
                automapConfig.CreateMap<Business.DomainModel.Restaurant, Restaurant>()
                    .ForMember(dest => dest.CuisineId, opt => opt.MapFrom(src => src.Cuisine.HasValue ? (int?)src.Cuisine.Value : null));

                // mapping from Db model to Domain model
                automapConfig.CreateMap<Restaurant, Business.DomainModel.Restaurant>()
                    .ForMember(dest => dest.Cuisine, opt => opt.MapFrom(src => src.CuisineId.HasValue ? (CuisineEnum?)src.CuisineId.Value : null));
            }
        }
    }
}
