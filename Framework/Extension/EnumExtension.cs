﻿using System;
using System.ComponentModel;

namespace Framework.Extension
{
    public static class EnumExtension
    {
        /// <summary>
        ///   Get the description string from the Description attribute (if exists)
        /// 
        ///   (http://stackoverflow.com/questions/79126/create-generic-method-constraining-t-to-an-enum)
        /// </summary>
        /// <param name = "enumValue">The enum value to be described</param>
        /// <returns></returns>
        public static string GetDescription<T>(this T enumValue) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            var fi = typeof(T).GetField(enumValue.ToString());

            var attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;

            return enumValue.ToString();
        }
    }
}
