﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class Entities : DbContext
    {
        public Entities(string connectionString)
            : base(connectionString)
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Restaurant> Restaurants { get; set; }
    
        public virtual int UpdateRestaurant(Nullable<int> id, string title, string phoneNumberText, Nullable<long> phoneNumberKey, Nullable<int> cuisineId, string headChefName, Nullable<byte> ratingFood, Nullable<byte> ratingWine, string streetAddress, string addressSuburb, string addressState, string addressPostCode, Nullable<double> latitude, Nullable<double> longitude, string webAddress, string reviewText, byte[] photo)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            var titleParameter = title != null ?
                new ObjectParameter("title", title) :
                new ObjectParameter("title", typeof(string));
    
            var phoneNumberTextParameter = phoneNumberText != null ?
                new ObjectParameter("phoneNumberText", phoneNumberText) :
                new ObjectParameter("phoneNumberText", typeof(string));
    
            var phoneNumberKeyParameter = phoneNumberKey.HasValue ?
                new ObjectParameter("phoneNumberKey", phoneNumberKey) :
                new ObjectParameter("phoneNumberKey", typeof(long));
    
            var cuisineIdParameter = cuisineId.HasValue ?
                new ObjectParameter("cuisineId", cuisineId) :
                new ObjectParameter("cuisineId", typeof(int));
    
            var headChefNameParameter = headChefName != null ?
                new ObjectParameter("headChefName", headChefName) :
                new ObjectParameter("headChefName", typeof(string));
    
            var ratingFoodParameter = ratingFood.HasValue ?
                new ObjectParameter("ratingFood", ratingFood) :
                new ObjectParameter("ratingFood", typeof(byte));
    
            var ratingWineParameter = ratingWine.HasValue ?
                new ObjectParameter("ratingWine", ratingWine) :
                new ObjectParameter("ratingWine", typeof(byte));
    
            var streetAddressParameter = streetAddress != null ?
                new ObjectParameter("streetAddress", streetAddress) :
                new ObjectParameter("streetAddress", typeof(string));
    
            var addressSuburbParameter = addressSuburb != null ?
                new ObjectParameter("addressSuburb", addressSuburb) :
                new ObjectParameter("addressSuburb", typeof(string));
    
            var addressStateParameter = addressState != null ?
                new ObjectParameter("addressState", addressState) :
                new ObjectParameter("addressState", typeof(string));
    
            var addressPostCodeParameter = addressPostCode != null ?
                new ObjectParameter("addressPostCode", addressPostCode) :
                new ObjectParameter("addressPostCode", typeof(string));
    
            var latitudeParameter = latitude.HasValue ?
                new ObjectParameter("latitude", latitude) :
                new ObjectParameter("latitude", typeof(double));
    
            var longitudeParameter = longitude.HasValue ?
                new ObjectParameter("longitude", longitude) :
                new ObjectParameter("longitude", typeof(double));
    
            var webAddressParameter = webAddress != null ?
                new ObjectParameter("webAddress", webAddress) :
                new ObjectParameter("webAddress", typeof(string));
    
            var reviewTextParameter = reviewText != null ?
                new ObjectParameter("reviewText", reviewText) :
                new ObjectParameter("reviewText", typeof(string));
    
            var photoParameter = photo != null ?
                new ObjectParameter("photo", photo) :
                new ObjectParameter("photo", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateRestaurant", idParameter, titleParameter, phoneNumberTextParameter, phoneNumberKeyParameter, cuisineIdParameter, headChefNameParameter, ratingFoodParameter, ratingWineParameter, streetAddressParameter, addressSuburbParameter, addressStateParameter, addressPostCodeParameter, latitudeParameter, longitudeParameter, webAddressParameter, reviewTextParameter, photoParameter);
        }
    }
}
