﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using Business.DomainModel;
using Business.Repository;
using BusinessService;

namespace BusinessServiceTest
{
    [TestClass]
    public class RestaurantServiceTest
    {
        private Mock<IRestaurantRepository> _restaurantRepositoryMock;
        private RestaurantService _restaurantService;
        private Restaurant _restaurant;

        [TestInitialize]
        public void TestInit()
        {
            _restaurantRepositoryMock = new Mock<IRestaurantRepository>();

            _restaurantService = new RestaurantService(_restaurantRepositoryMock.Object);

            _restaurant = new Restaurant();
        }

        [TestMethod]
        public void TestFormatPhoneNumber_NormalFormat()
        {
            _restaurant.PhoneNumberText = "(02) 9999 0000";

            _restaurantService.FormatPhoneNumber(_restaurant);

            Assert.AreEqual<string>("(02) 9999 0000", _restaurant.PhoneNumberText);
            Assert.AreEqual<long>(299990000, _restaurant.PhoneNumberKey);
        }

        [TestMethod]
        public void TestFormatPhoneNumber_ExtraLeadZero()
        {
            _restaurant.PhoneNumberText = "000299990000";

            _restaurantService.FormatPhoneNumber(_restaurant);

            Assert.AreEqual<string>("(02) 9999 0000", _restaurant.PhoneNumberText);
            Assert.AreEqual<long>(299990000, _restaurant.PhoneNumberKey);
        }

        [TestMethod]
        public void TestFormatPhoneNumber_ValidNoBrackets()
        {
            _restaurant.PhoneNumberText = "299990000";

            _restaurantService.FormatPhoneNumber(_restaurant);

            Assert.AreEqual<string>("(02) 9999 0000", _restaurant.PhoneNumberText);
            Assert.AreEqual<long>(299990000, _restaurant.PhoneNumberKey);
        }

        [TestMethod]
        public void TestFormatPhoneNumber_ValidWithOtherChars()
        {
            _restaurant.PhoneNumberText = " x2x9999 x0000xx x";

            _restaurantService.FormatPhoneNumber(_restaurant);

            Assert.AreEqual<string>("(02) 9999 0000", _restaurant.PhoneNumberText);
            Assert.AreEqual<long>(299990000, _restaurant.PhoneNumberKey);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void TestFormatPhoneNumber_8Significant()
        {
            _restaurant.PhoneNumberText = "029999000";

            _restaurantService.FormatPhoneNumber(_restaurant);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void TestFormatPhoneNumber_MissingOneDigit()
        {
            _restaurant.PhoneNumberText = "(02) 9999 000";

            _restaurantService.FormatPhoneNumber(_restaurant);
        }

        [TestMethod]
        public void TestFormatPhoneNumber_ExtraOneDigit()
        {
            _restaurant.PhoneNumberText = "(02) 9999 0000 1";

            _restaurantService.FormatPhoneNumber(_restaurant);

            Assert.AreEqual<string>("(02) 9999 0000", _restaurant.PhoneNumberText);
            Assert.AreEqual<long>(299990000, _restaurant.PhoneNumberKey);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void TestFormatPhoneNumber_NullValue()
        {
            _restaurant.PhoneNumberText = null;

            _restaurantService.FormatPhoneNumber(_restaurant);
        }

        [TestMethod]
        public void TestValidateRestaurant_BothProvided()
        {
            _restaurant.Title = "test title";
            _restaurant.PhoneNumberText = "xxx";

            _restaurantService.ValidateRestaurant(_restaurant);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void TestValidateRestaurant_MissingTitle()
        {
            _restaurant.Title = null;
            _restaurant.PhoneNumberText = "xxx";

            _restaurantService.ValidateRestaurant(_restaurant);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void TestValidateRestaurant_MissingPhone()
        {
            _restaurant.Title = "test title";
            _restaurant.PhoneNumberText = null;

            _restaurantService.ValidateRestaurant(_restaurant);
        }
    }
}
