﻿using Ninject;
using Ninject.Infrastructure.Disposal;
using Ninject.Parameters;
using Ninject.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace MvcApplication.App_Start
{
    /// <summary>
    /// Dependency Resolver implementation for ninject
    /// </summary>
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        public NinjectDependencyResolver(IResolutionRoot resolutionRoot)
            : base(resolutionRoot)
        {
        }

        public virtual IDependencyScope BeginScope()
        {
            return this;
        }
    }

    /// <summary>
    /// Dependency Scope implementation for ninject
    /// </summary>
    public class NinjectDependencyScope : DisposableObject, IDependencyScope
    {
        public NinjectDependencyScope(IResolutionRoot resolutionRoot)
        {
            this.ResolutionRoot = resolutionRoot;
        }

        protected IResolutionRoot ResolutionRoot
        {
            get;
            private set;
        }

        public object GetService(Type serviceType)
        {
            var request = this.ResolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return this.ResolutionRoot.Resolve(request).SingleOrDefault();
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.ResolutionRoot.GetAll(serviceType).ToList();
        }
    }
}