﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Business.DomainModel;

namespace Business.Service
{
    public interface IRestaurantService
    {
        Restaurant Update(Restaurant restaurant);
    }
}
