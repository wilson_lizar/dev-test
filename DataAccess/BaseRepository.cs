﻿using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public abstract class BaseRepository
    {
        private readonly string _efConnectionString = null;

        public BaseRepository(string connectionString)
        {
            // ref: http://msdn.microsoft.com/en-us/library/bb738533.aspx
            var entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = connectionString;
            entityBuilder.Metadata = @"res://*/RestaurantDb.csdl|res://*/RestaurantDb.ssdl|res://*/RestaurantDb.msl";

            _efConnectionString = entityBuilder.ToString();
        }

        protected Entities GetEntities()
        {
            return new Entities(_efConnectionString);
        }
    }
}
