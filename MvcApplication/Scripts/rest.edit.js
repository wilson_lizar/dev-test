﻿var restedit = {
    map: null,
    geocoder: null,
    marker: null,
    review: null,
    init: function () {
        var restLat = $('#Latitude').val();
        var restLon = $('#Longitude').val();
        var hasLoc = restLat != '' && restLon != '';

        restedit.geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(hasLoc ? restLat : -34.397, hasLoc ? restLon : 150.644);
        var mapOptions = {
            zoom: hasLoc ? 15 : 8,
            center: latlng
        }

        restedit.map = new google.maps.Map(document.getElementById('locMap'), mapOptions);

        if (hasLoc) {
            restedit.marker = new google.maps.Marker({
                map: restedit.map,
                position: latlng
            });
        }

        $('#btnLocate').click(function (evnt) {
            evnt.preventDefault();
            $('#locErr').text('');

            var street = $('#StreetAddress').val().trim();
            var suburb = $('#AddressSuburb').val().trim();
            var state = $('#AddressState').val().trim();
            if (street != '' && suburb != '' && state != '') {
                var address = street + ', ' + suburb + ', ' + state;

                restedit.geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (restedit.marker)
                            restedit.marker.setMap(null); // detach from the map

                        restedit.marker = new google.maps.Marker({
                            map: restedit.map,
                            position: results[0].geometry.location
                        });
                        restedit.map.setCenter(results[0].geometry.location);
                        restedit.map.setZoom(15);

                        // update hidden fields
                        $('#Longitude').val(results[0].geometry.location.A);
                        $('#Latitude').val(results[0].geometry.location.k);

                    } else {
                        $('#locErr').text('Unable to locate the restaurant address.');
                    }
                });
            }
            else {
                $('#locErr').text('Please enter the Street address, suburb and state fields to locate the restaurant.');
            }
        });

        $('#ReviewText').cleditor();
    }
};

$(document).ready(function () {
    restedit.init();
});