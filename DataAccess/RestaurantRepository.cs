﻿using Business.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Business.DomainModel;
using AutoMapper;

namespace DataAccess
{
    public class RestaurantRepository : BaseRepository, IRestaurantRepository
    {
        private IMappingEngine _mappingEngine = null;

        public RestaurantRepository(string connectionString, IMappingEngine mappingEngine)
            : base(connectionString)
        {
            _mappingEngine = mappingEngine;
        }

        public IEnumerable<Business.DomainModel.Restaurant> GetAll()
        {
            using (var entities = GetEntities())
            {
                var dbRestaurants = entities.Restaurants.ToList();
                var result = dbRestaurants
                    .Select(r => _mappingEngine.Map<Business.DomainModel.Restaurant>(r));

                return result;
            }
        }

        public Business.DomainModel.Restaurant Get(int id)
        {
            using (var entities = GetEntities())
            {
                var dbRestaurant = entities.Restaurants.Where(r => r.Id == id).FirstOrDefault();

                return dbRestaurant != null ? _mappingEngine.Map<Business.DomainModel.Restaurant>(dbRestaurant) : null;
            }
        }

        public bool Update(Business.DomainModel.Restaurant restaurant)
        {
            var dbRestaurant = _mappingEngine.Map<Restaurant>(restaurant);

            using (var entities = GetEntities())
            {
                var result = entities.UpdateRestaurant(dbRestaurant.Id,
                    dbRestaurant.Title,
                    dbRestaurant.PhoneNumberText,
                    dbRestaurant.PhoneNumberKey,
                    dbRestaurant.CuisineId,
                    dbRestaurant.HeadChefName,
                    dbRestaurant.RatingFood,
                    dbRestaurant.RatingWine,
                    dbRestaurant.StreetAddress,
                    dbRestaurant.AddressSuburb,
                    dbRestaurant.AddressState,
                    dbRestaurant.AddressPostCode,
                    dbRestaurant.Latitude,
                    dbRestaurant.Longitude,
                    dbRestaurant.WebAddress,
                    dbRestaurant.ReviewText,
                    dbRestaurant.Photo);

                return result > 0;
            }
        }
    }
}
