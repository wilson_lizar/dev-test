﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.DomainModel;
using Business.Repository;
using Business.Service;

using AutoMapper;

namespace MvcApplication.Controllers
{
    public class RestaurantController : Controller
    {
        private IMappingEngine _mappingEngine;
        private IRestaurantRepository _restaurantRepository;
        private IRestaurantService _restaurantService;

        public RestaurantController(IMappingEngine mappingEngine,
            IRestaurantRepository restaurantRepository,
            IRestaurantService restaurantService)
        {
            _mappingEngine = mappingEngine;
            _restaurantRepository = restaurantRepository;
            _restaurantService = restaurantService;
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var restaurant = _restaurantRepository.Get(id);

            if (restaurant == null) // restaurant not found
                return RedirectToAction("Index", "Home");

            return View(_mappingEngine.Map<Models.RestaurantModel>(restaurant));
        }

        [HttpGet]
        public ActionResult Photo(int id)
        {
            var restaurant = _restaurantRepository.Get(id);

            var data = new byte[0];
            if (restaurant != null && restaurant.Photo != null)
            {
                data = restaurant.Photo;
            }
            return File(data, "image/jpeg");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Models.RestaurantModel restaurantVm)
        {
            if (ModelState.IsValid)
            {
                // validate file and it's size
                if (restaurantVm.Photo != null)
                {
                    if (!restaurantVm.Photo.ContentType.Equals("image/jpeg", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ViewBag.ErrorMessage = "Only JPEG photos are supported, please upload a JPEG instead.";
                        return View(restaurantVm);
                    }

                    if (restaurantVm.Photo.ContentLength > 500 * 1024)
                    {
                        ViewBag.ErrorMessage = "Photo has to be less than 500KB.";
                        return View(restaurantVm);
                    }
                }

                var restaurant = _mappingEngine.Map<Restaurant>(restaurantVm);
                restaurant.Id = id;

                try
                {
                    restaurant = _restaurantService.Update(restaurant);

                    ViewBag.Message = String.Format("Saved '{0}'.", restaurant.Title);
                    ModelState.Clear();
                    restaurantVm = _mappingEngine.Map<Models.RestaurantModel>(restaurant);
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                }
            }

            return View(restaurantVm);
        }
    }
}
