﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AutoMapper;
using Business.Repository;

namespace MvcApplication.Controllers
{
    public class HomeController : Controller
    {
        private IMappingEngine _mappingEngine;
        private IRestaurantRepository _restaurantRepository;

        public HomeController(IMappingEngine mappingEngine, IRestaurantRepository restaurantRepository)
        {
            _mappingEngine = mappingEngine;
            _restaurantRepository = restaurantRepository;
        }

        public ActionResult Index()
        {
            var restaurants = _restaurantRepository.GetAll().ToList();

            return View(restaurants.Select(r => _mappingEngine.Map<Models.RestaurantModel>(r)).ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
