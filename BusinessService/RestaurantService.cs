﻿using Business.DomainModel;
using Business.Repository;
using Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BusinessService
{
    public class RestaurantService : IRestaurantService
    {
        private IRestaurantRepository _restaurantRepository;

        public RestaurantService(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;
        }

        public Restaurant Update(Restaurant restaurant)
        {
            FormatPhoneNumber(restaurant);

            ValidateRestaurant(restaurant);

            var result = _restaurantRepository.Update(restaurant);

            if (!result)
                throw new ArgumentException("Restaurant not found, no record was updated");

            restaurant = _restaurantRepository.Get(restaurant.Id);

            return restaurant;
        }

        public void FormatPhoneNumber(Restaurant restaurant)
        {
            var phoneNumberString = restaurant.PhoneNumberText;

            // strip out non digits
            phoneNumberString = Regex.Replace(phoneNumberString, @"\D", string.Empty);

            // trim any leading 0 to get the most significant number
            phoneNumberString = phoneNumberString.TrimStart('0');

            if (phoneNumberString.Length < 9)
                throw new ArgumentException("Phone number is less than 9 digits");

            // only look at the first 9 digits
            phoneNumberString = phoneNumberString.Substring(0, 9);

            // convert to long
            var phoneNumberLong = long.Parse(phoneNumberString);

            phoneNumberString = string.Format("(0{0}) {1} {2}", phoneNumberString.Substring(0, 1), phoneNumberString.Substring(1, 4), phoneNumberString.Substring(5));

            restaurant.PhoneNumberText = phoneNumberString;
            restaurant.PhoneNumberKey = phoneNumberLong;
        }

        public void ValidateRestaurant(Restaurant restaurant)
        {
            if (string.IsNullOrWhiteSpace(restaurant.Title))
                throw new ArgumentException("Restaurant Title is required");

            if (string.IsNullOrWhiteSpace(restaurant.PhoneNumberText))
                throw new ArgumentException("Restaurant Phone number is required");
        }
    }
}
