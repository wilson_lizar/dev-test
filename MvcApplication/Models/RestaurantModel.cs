﻿using System.ComponentModel.DataAnnotations;
using Business.DomainModel;
using System.Web.Mvc;
using System.Web;

namespace MvcApplication.Models
{
    public enum RatingEnum : byte
    {
        Bad = 0,
        Moderate = 1,
        Good = 2,
        Excellent = 3
    }

    public class RestaurantModel
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        public string PhoneNumberText { get; set; }

        public CuisineEnum? Cuisine { get; set; }

        [Display(Name = "Head chef")]
        public string HeadChefName { get; set; }

        [Display(Name = "Food rating")]
        public byte? RatingFood { get; set; }

        [Display(Name = "Wine rating")]
        public byte? RatingWine { get; set; }

        [Display(Name = "Street address")]
        public string StreetAddress { get; set; }

        [Display(Name = "Suburb")]
        public string AddressSuburb { get; set; }

        [Display(Name = "State")]
        public string AddressState { get; set; }

        [Display(Name = "Postcode")]
        public string AddressPostCode { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        [DataAnnotationsExtensions.Url(urlOptions: DataAnnotationsExtensions.UrlOptions.OptionalProtocol)]
        [Display(Name = "Website address")]
        public string WebAddress { get; set; }

        [AllowHtml]
        public string ReviewText { get; set; }

        public HttpPostedFileBase Photo { get; set; }

        public bool HasPhoto { get; set; }
    }
}