﻿using System.ComponentModel;

namespace Business.DomainModel
{
    public enum CuisineEnum
    {
        Asian = 1,
        Chinese = 2,
        English = 3,
        European = 4,
        French = 5,
        Greek = 6,
        Indian = 7,
        Indonesian = 8,
        Italian = 9,
        Japanese = 10,
        Korean = 11,
        [Description("Latin American")]
        LatinAmerican = 12,
        Lebanese = 13,
        Malaysian = 14,
        Mediterranean = 15,
        Mexican = 16,
        [Description("Middle Eastern")]
        MiddleEastern = 17,
        [Description("Modern Australian")]
        ModernAustralian = 18,
        [Description("Modern European")]
        ModernEuropean = 19,
        Seafood = 20,
        Spanish = 21,
        Steak = 22,
        Thai = 23,
        Turkish = 24,
        Vietnamese = 25,
        Argentine = 26,
        [Description("Modern Asian")]
        ModernAsian = 27,
        [Description("North African")]
        NorthAfrican = 28,
        Pizza = 29,
        American = 30
    }
}
